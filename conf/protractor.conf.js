exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['test/index.spec.js'],
  rootElement: '[ng-app]'
  // specs: ['src/index.spec.js']
};
