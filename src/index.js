var angular = require('angular');
require('angular-mocks');

var signupForm = require('./app/signupForm.js');
require('angular-ui-router');
var routesConfig = require('./routes.js');
var passwordValidations = require('./app/passwordValidations.js');
require('./index.scss');

var app = 'app';
module.exports = app;

angular
  .module(app, ['ui.router', 'ngMockE2E'])
  .config(routesConfig)
  .component('app', signupForm)

  .directive('straight3', passwordValidations.straight3)
  .directive('iol', passwordValidations.iol)
  .directive('pairs2', passwordValidations.pairs2)
  .directive('lowercase', passwordValidations.lowercase)
  .directive('length32', passwordValidations.length32);
