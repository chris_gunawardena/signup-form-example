module.exports = {
  straight3: function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.straight3 = function (modelValue, viewValue) {
          viewValue = viewValue || '';
          for (var i = 1; i < viewValue.length - 1; i++) {
            if (viewValue.charCodeAt(i - 1) + 1 === viewValue.charCodeAt(i) &&
                viewValue.charCodeAt(i) === viewValue.charCodeAt(i + 1) - 1) {
              return true;
            }
          }
          return false;
        };
      }
    };
  },
  iol: function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.iol = function (modelValue, viewValue) {
          return !/([iol]+)/.test(viewValue || '');
        };
      }
    };
  },
  pairs2: function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.pairs2 = function (modelValue, viewValue) {
          return /[a-z]*([a-z])\1[a-z]*([a-z])\2[a-z]*/.test(viewValue || '');
        };
      }
    };
  },
  lowercase: function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.lowercase = function (modelValue, viewValue) {
          return /^[a-z]+$/.test(viewValue || '');
        };
      }
    };
  },
  length32: function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.length32 = function (modelValue, viewValue) {
          return (viewValue || '').length <= 32;
        };
      }
    };
  }
};
