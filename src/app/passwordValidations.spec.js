var passwordValidations = require('./passwordValidations.js');

describe('signup form validations', function () {
  describe('straight3', function () {
    it('should return true if string includes one increasing straight of at least three letters', function () {
      var straight3 = passwordValidations.straight3();
      var ctrl = {$validators: {}};
      straight3.link(null, null, null, ctrl);

      expect(ctrl.$validators.straight3(null, null)).toEqual(false);
      expect(ctrl.$validators.straight3(null, '')).toEqual(false);
      expect(ctrl.$validators.straight3(null, 'aabcc')).toEqual(true);
      expect(ctrl.$validators.straight3(null, 'xaabccz')).toEqual(true);
      expect(ctrl.$validators.straight3(null, 'axbxc')).toEqual(false);
      expect(ctrl.$validators.straight3(null, 'abd')).toEqual(false);
    });
  });
  describe('iol', function () {
    it('should return true if string does not include i, o  ir l', function () {
      var straight3 = passwordValidations.iol();
      var ctrl = {$validators: {}};
      straight3.link(null, null, null, ctrl);

      expect(ctrl.$validators.iol(null, null)).toEqual(true);
      expect(ctrl.$validators.iol(null, '')).toEqual(true);
      expect(ctrl.$validators.iol(null, 'aabcc')).toEqual(true);
      expect(ctrl.$validators.iol(null, 'xaabccz')).toEqual(true);
      expect(ctrl.$validators.iol(null, 'axbxic')).toEqual(false);
      expect(ctrl.$validators.iol(null, 'abod')).toEqual(false);
      expect(ctrl.$validators.iol(null, 'abld')).toEqual(false);
      expect(ctrl.$validators.iol(null, 'abiold')).toEqual(false);
    });
  });
  describe('pairs2', function () {
    it('should return true if string includes at least two non-overlapping pairs of letter', function () {
      var pairs2 = passwordValidations.pairs2();
      var ctrl = {$validators: {}};
      pairs2.link(null, null, null, ctrl);

      expect(ctrl.$validators.pairs2(null, null)).toEqual(false);
      expect(ctrl.$validators.pairs2(null, '')).toEqual(false);
      expect(ctrl.$validators.pairs2(null, 'aabcc')).toEqual(true);
      expect(ctrl.$validators.pairs2(null, 'xaabccz')).toEqual(true);
      expect(ctrl.$validators.pairs2(null, 'aaaxbxcb')).toEqual(false);
      expect(ctrl.$validators.pairs2(null, 'abbacd')).toEqual(false);
    });
  });
  describe('lowercase', function () {
    it('should return true if all chars are lower case', function () {
      var lowercase = passwordValidations.lowercase();
      var ctrl = {$validators: {}};
      lowercase.link(null, null, null, ctrl);

      expect(ctrl.$validators.lowercase(null, null)).toEqual(false);
      expect(ctrl.$validators.lowercase(null, '')).toEqual(false);
      expect(ctrl.$validators.lowercase(null, 'aabcc')).toEqual(true);
      expect(ctrl.$validators.lowercase(null, 'xaabccz')).toEqual(true);
      expect(ctrl.$validators.lowercase(null, 'axAbxc')).toEqual(false);
      expect(ctrl.$validators.lowercase(null, 'abdABC')).toEqual(false);
    });
  });
  describe('length32', function () {
    it('should return true if string is less then 32 chars', function () {
      var length32 = passwordValidations.length32();
      var ctrl = {$validators: {}};
      length32.link(null, null, null, ctrl);

      expect(ctrl.$validators.length32(null, null)).toEqual(true);
      expect(ctrl.$validators.length32(null, '')).toEqual(true);
      expect(ctrl.$validators.length32(null, 'aabcc')).toEqual(true);
      expect(ctrl.$validators.length32(null, 'xaabccz')).toEqual(true);
      expect(ctrl.$validators.length32(null, 'axbxc')).toEqual(true);
      expect(ctrl.$validators.length32(null, 'absdfasdfsfgafggdfkghksdfhgksdhfgksdhfgjhsdfgd')).toEqual(false);
    });
  });
});
