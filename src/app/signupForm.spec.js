var angular = require('angular');
require('angular-mocks');
var signupForm = require('./signupForm');

describe('signup form elements', function () {
  beforeEach(function () {
    angular
      .module('signupForm', ['app/signupForm.html'])
      .component('signupForm', signupForm);
    angular.mock.module('signupForm');
  });

  it('should render signup form email field', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<signup-form>Loading...</signup-form>')($rootScope);
    $rootScope.$digest();

    var emailInput = element[0].querySelectorAll('input#inputEmail');
    expect(emailInput.length).toEqual(1);
    expect(emailInput[0].getAttribute('type')).toEqual('email');
    expect(emailInput[0].getAttribute('placeholder')).toEqual('Email');
  }));

  it('should render signup form password fields', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<signup-form>Loading...</signup-form>')($rootScope);
    $rootScope.$digest();

    var passwordInput1 = element[0].querySelectorAll('input#inputPassword1');
    expect(passwordInput1.length).toEqual(1);
    expect(passwordInput1[0].getAttribute('type')).toEqual('password');
    expect(passwordInput1[0].getAttribute('placeholder')).toEqual('New password');
    var passwordInput2 = element[0].querySelectorAll('input#inputPassword2');
    expect(passwordInput2.length).toEqual(1);
    expect(passwordInput2[0].getAttribute('type')).toEqual('password');
    expect(passwordInput2[0].getAttribute('placeholder')).toEqual('Confirm new password');
  }));

  it('should render signup form terms and conditions checkbox', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<signup-form>Loading...</signup-form>')($rootScope);
    $rootScope.$digest();

    var agreeCheckbox = element[0].querySelectorAll('input[type=checkbox]#agree');
    expect(agreeCheckbox.length).toEqual(1);
    expect(agreeCheckbox[0].getAttribute('checked')).toEqual(null);
  }));

  it('should render signup form submit buttons', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<signup-form>Loading...</signup-form>')($rootScope);
    $rootScope.$digest();

    var submitButton = element[0].querySelectorAll('button[type=submit]');
    expect(submitButton.length).toEqual(1);
    expect(submitButton[0].getAttribute('type')).toEqual('submit');
    expect(submitButton[0].innerText).toEqual('Next');
  }));
});
