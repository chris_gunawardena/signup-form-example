module.exports = {
  template: require('./signupForm.html'),
  controller: ['$http', '$httpBackend', '$scope', SignupFormController]
};

function SignupFormController($http, $httpBackend, $scope) {
  // initial state
  $scope.state = 'data-entry';

  /** @function submit
   *  Submit the sugnup form using $http.post and set state
   */
  this.submit = function () {
    // Mock backend
    $httpBackend.whenPOST('/register').respond(function (method, url, data) {
      var postData = angular.fromJson(data);
      var statusCode = parseInt(postData.email.split('@')[0], 10);
      if (isNaN(statusCode) || statusCode < 500 || statusCode > 599) {
        statusCode = 201;
      }
      return [statusCode, data];
    });

    // Hide form to prevent doubleups
    $scope.state = 'in-progress';
    $http({
      method: 'POST',
      url: '/register',
      data: {
        email: $scope.email,
        password: $scope.password1
      }
    }, {
      timeout: 2000
    }).then(function successCallback() {
      $scope.state = 'success';
    }, function errorCallback() {
      $scope.state = 'error';
    });
  };
}
