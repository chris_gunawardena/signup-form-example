var gulp = require('gulp');
var gulpProtractorAngular = require('gulp-angular-protractor');

gulp.task('e2e', protractor);

function protractor(done) {
    gulp
        .src(['e2e/index.spec.js'])
        .pipe(gulpProtractorAngular({
            'configFile': 'conf/protractor.conf.js',
            'debug': false,
            'autoStartStopServer': true
        }))
        .on('error', function(e) {
            console.log(e);
        })
        .on('end', done);
}
