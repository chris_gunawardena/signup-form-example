// from https://github.com/angular/protractor/blob/master/example/example_spec.js

describe('signup page', function () {
  var emailInput;
  var password1Input;
  var password2Input;
  var agreeCheckbox;
  var submitButton;
  var successMessage;
  var errorMessage;

  beforeEach(function () {
    browser.get('http://localhost:3000/');
    emailInput = element(by.model('email'));
    password1Input = element(by.model('password1'));
    password2Input = element(by.model('password2'));
    agreeCheckbox = element(by.css('input[type="checkbox"]'));
    submitButton = element(by.css('button[type="submit"]'));
    successMessage = element(by.css('div.alert-success'));
    errorMessage = element(by.css('div.alert-danger'));
  });

  it('should see the signup page with all the requred elements', function () {
    browser.get('http://localhost:3000/');

    expect(emailInput.isPresent()).toBe(true);
    expect(password1Input.isPresent()).toBe(true);
    expect(password2Input.isPresent()).toBe(true);
    expect(agreeCheckbox.isPresent()).toBe(true);
    expect(submitButton.isPresent()).toBe(true);
  });

  describe('successful signup', function () {
    it('should give successful confirmation', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabcc');
      password2Input.sendKeys('aabcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(successMessage.isDisplayed()).toBe(true);
      expect(errorMessage.isDisplayed()).toBe(false);
    });
  });

  describe('unsuccessful signup', function () {
    it('should give error message when server error', function () {
      emailInput.sendKeys('500@test.com');
      password1Input.sendKeys('aabcc');
      password2Input.sendKeys('aabcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(true);
    });

    it('should give not allow to submit when email is missing', function () {
      password1Input.sendKeys('aabcc');
      password2Input.sendKeys('aabcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password1 is missing', function () {
      emailInput.sendKeys('test@test.com');
      password2Input.sendKeys('aabcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password2 is missing', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when passwords are not matching', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabcc');
      password2Input.sendKeys('aabcca');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password doesn\'t have  increasing straight of at least three letters', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabbcc');
      password2Input.sendKeys('aabbcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password contains letters i, o or l', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabcci');
      password2Input.sendKeys('aabcci');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password contains letters i, o or l', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabcco');
      password2Input.sendKeys('aabcco');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password contains letters i, o or l', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabccl');
      password2Input.sendKeys('aabccl');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password doesn\'t have two non-overlapping pairs of letters', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aaabc');
      password2Input.sendKeys('aaabc');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password is longer then 32 letters', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabccaabccaabccaabccaabccaabccaabcc');
      password2Input.sendKeys('aabccaabccaabccaabccaabccaabccaabcc');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when password non lower case letters', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabccA');
      password2Input.sendKeys('aabccA');
      agreeCheckbox.click();
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });

    it('should give not allow to submit when agree is not checked', function () {
      emailInput.sendKeys('test@test.com');
      password1Input.sendKeys('aabcc');
      password2Input.sendKeys('aabcc');
      submitButton.click();

      expect(submitButton.isEnabled()).toBe(false);
      expect(successMessage.isDisplayed()).toBe(false);
      expect(errorMessage.isDisplayed()).toBe(false);
    });
  });
});
